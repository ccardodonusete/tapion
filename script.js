document.addEventListener('DOMContentLoaded', function () {
    const hoverArea = document.getElementById('hover-area');
    const audio = document.getElementById('background-audio');

    hoverArea.addEventListener('mouseover', function () {
        audio.play();
    });

    hoverArea.addEventListener('mouseout', function () {
        audio.pause();
        audio.currentTime = 0; // Reset the audio to the beginning
    });
});
